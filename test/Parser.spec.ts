import {beforeEach, describe, expect, test} from '@jest/globals';
import {ILexer, Lexer} from "../src/Lexer";
import {SchemeNumber, SchemeVoid} from "../src/Datatype";
import {Parser} from "../src/Parser";
import {ConditionalExpr, DefinitionExpr, LiteralExpr, ProcedureCallExpr, VariableExpr} from "../src/Expr";
import {Token, TokenType} from "../src/Token";

describe('Parser', () => {
	let parser: Parser;
	let lexer: ILexer;

	beforeEach(() => {
		parser = new Parser();
		lexer = new Lexer();
	})

	test('empty', () => {
		expect(parser.parse([])).toStrictEqual([]);
	});

	test('parse identifier', () => {
		expect(parser.parse(lexer.lex("something"))).toStrictEqual([
			new VariableExpr(lexer.lex("something")[0])
		]);
	});

	test('unbalanced parentheses', () => {
		const tokens = lexer.lex("(define something (hello) hello))")
		expect(() => parser.parse(tokens)).toThrow()
	})

	test('parse literal', () => {
		const tokens = lexer.lex("1234")
		expect(parser.parse(tokens)).toStrictEqual([
			new LiteralExpr(new SchemeNumber(1234))
		])
	})

	test('simple application', () => {
		const tokens = lexer.lex("(a b)")
		expect(parser.parse(tokens)).toStrictEqual([
			new ProcedureCallExpr(
				new VariableExpr(new Token(TokenType.Identifier, "a", new SchemeVoid(), 0)),
				[new VariableExpr(new Token(TokenType.Identifier, "b", new SchemeVoid(), 0))])
		])
	})

	test('new line function application', () => {
		const tokens = lexer.lex("(a\nb)")
		expect(parser.parse(tokens)).toStrictEqual([
			new ProcedureCallExpr(
				new VariableExpr(new Token(TokenType.Identifier, "a", new SchemeVoid(), 0)),
				[new VariableExpr(new Token(TokenType.Identifier, "b", new SchemeVoid(), 1))])
		])
	})

	test("unclosed parentheses", () => {
		const tokens = lexer.lex("(print hello world")
		expect(() => parser.parse(tokens)).toThrow("Unexpected end of file when parsing expression")
	})

	test("if expression without alternate", () => {
		const tokens = lexer.lex("(if a b)")
		expect(parser.parse(tokens)).toStrictEqual([
			new ConditionalExpr(
				VariableExpr.construct("a"),
				VariableExpr.construct("b"))
		])
	})

	test("if expression", () => {
		const tokens = lexer.lex("(if a b c)")
		expect(parser.parse(tokens)).toStrictEqual([
			new ConditionalExpr(
				VariableExpr.construct("a"),
				VariableExpr.construct("b"),
				VariableExpr.construct("c"))
		])
	})

	test("define statement", () => {
		const tokens = lexer.lex("(define a 1)")
		expect(parser.parse(tokens)).toStrictEqual([
			new DefinitionExpr(
				new Token(TokenType.Identifier, "a", new SchemeVoid(), 0),
				new LiteralExpr(new SchemeNumber(1))
			)
		])

	})
});
