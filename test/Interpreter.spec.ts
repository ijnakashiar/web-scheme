import {beforeEach, describe, expect, test} from '@jest/globals';
import {IInterpreter, SchemeInterpreter} from "../src/Interpreter";
import {SchemeNumber, SchemeObject, SchemeProcedure, SchemeVoid} from "../src/Datatype";
import { Lexer } from '../src/Lexer';
import { Parser } from '../src/Parser';
import { Environment } from '../src/Environment';

describe('Interpreter', ()=>{
    let interpreter: IInterpreter<SchemeObject>
	let empty_interpreter: IInterpreter<SchemeObject>
	const input = (src: string) => (new Parser().parse(new Lexer().lex(src)))

    beforeEach(() => {
        interpreter = new SchemeInterpreter()
		empty_interpreter = new SchemeInterpreter(new Environment())
    })

    test('empty input', () => {
        expect(interpreter.interpret([])).toStrictEqual([])
    })

	test('test arithmetic', () => {
		const arith_expr = input("(+ 4 (* 2 (/ 2 (- 3 1))))")
		expect(interpreter.interpret(arith_expr)).toEqual([new SchemeNumber(6)])
	})

	// TODO: Eventually we'll want to type check before execution
	test('numeric type mismatch', () => {
		const expr1 = input("(+ #f 1)")
		const expr2 = input("(+ 1 #t)")
		expect(()=>interpreter.interpret(expr1)).toThrow("Expected number in argument 1")

		interpreter=new SchemeInterpreter()
		expect(()=>interpreter.interpret(expr2)).toThrow("Expected number in argument 2")
	})

	describe('mutable variables', () => {
		test('basic test', () => {
			const src=`(define x 10) (set! x 11) x`
			expect(interpreter.interpret(input(src))).toStrictEqual([new SchemeNumber(11)])
		})

		test('pass by value', () => {
			const src=`
(define x 11)
((lambda (f)
(set! f 14)) x)
x`
			expect(interpreter.interpret(input(src))).toStrictEqual([new SchemeNumber(14)])			

		})
	})


	describe('lambdas', ()=>{
		test('basic lambda', () => {
			const lambda_expr = input("(lambda (b) b)")
			expect(empty_interpreter.interpret(lambda_expr)).toEqual([SchemeProcedure.construct(["b"], "b")])
		})

		test('lambda application', () => {
			const expr = input("((lambda (b) b) 10)")
			expect(empty_interpreter.interpret(expr)).toEqual([new SchemeNumber(10)])
		})

		test('lambda returns lambda', () => {
			const expr = input("((lambda (b) b) (lambda (b) b))")
			expect(empty_interpreter.interpret(expr)).toStrictEqual([SchemeProcedure.construct(["b"], "b")])
		})

			test('operator not function', () => {
		expect(()=>empty_interpreter.interpret(input("(10 20)")))
			.toThrow("Operator is not a procedure")
	})

	test('operand length mismatch', () => {
		expect(()=>empty_interpreter.interpret(input("((lambda a a) 10 20)")))
			.toThrow("Operand length does not match")
	})

	})

	// TODO: Eventually we'll want to check this before interpreting
	test('undefined reference to variable', () => {
		expect(() => empty_interpreter.interpret(input("a")))
			.toThrow("Undefined reference")
	})

	test('if expression', () => {
		expect(
			empty_interpreter.interpret(input("(if #f 10 20)")))
			.toStrictEqual([new SchemeNumber(20)])

		expect(
			empty_interpreter.interpret(input("(if #t 10 20)")))
			.toStrictEqual([new SchemeNumber(10)])

		expect(
			empty_interpreter.interpret(input("(if 10 10)"))
		).toStrictEqual([new SchemeNumber(10)])
		
		expect(
			empty_interpreter.interpret(input("(if #f 10)"))
		).toStrictEqual([new SchemeVoid()])
	})

	describe('define', () => {
		test('simple define', () => {
		// As a result of how define is implemented, the program outputs void for define 'expressions'
		expect(empty_interpreter.interpret(input("(define a 10) a"))).toStrictEqual(
			[new SchemeVoid(), new SchemeNumber(10)]
		)			
		})

		test('define function', () => {
			expect(empty_interpreter.interpret(input("(define (a b c d) (+ b (+ c d))) a")))
				.toStrictEqual([new SchemeVoid(), SchemeProcedure.construct(["b", "c", "d"], "(+ b (+ c d))")])
		})
	})
})
