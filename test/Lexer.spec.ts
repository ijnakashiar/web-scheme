import {beforeEach, describe, expect, test} from '@jest/globals';
import {ILexer, Lexer} from "../src/Lexer";
import {Token, TokenType} from "../src/Token";
import {SchemeCharacter, SchemeNumber, SchemeString, SchemeVoid} from "../src/Datatype";

describe('Lexer', () => {
    let lexer: ILexer;

    beforeEach(()=> {
        lexer = new Lexer();
    })

    test('empty', () => {
        expect(lexer.lex("")).toStrictEqual([]);
    });

    test('seek to end', () => {
        expect(lexer.lex("a    ")).toStrictEqual([
            new Token(TokenType.Identifier, "a",new SchemeVoid(), 0)
        ])
    })

    test('basic', () => {
        expect(lexer.lex("(define hello 1)")).toStrictEqual([
            new Token(TokenType.OpenParen, "(",new SchemeVoid(), 0),
            new Token(TokenType.Identifier, "define",new SchemeVoid(), 0),
            new Token(TokenType.Identifier, "hello",new SchemeVoid(), 0),
            new Token(TokenType.Number, "1",new SchemeNumber(1), 0),
            new Token(TokenType.CloseParen, ")",new SchemeVoid(), 0),
        ])
    })

    test('string', () => {
        expect(lexer.lex("\"hello\"")).toStrictEqual([
            new Token(TokenType.String, "hello",new SchemeString("hello"), 0)
        ])
    })

    test('string within an expression', () => {
        expect(lexer.lex("(define \"hello\")")).toStrictEqual([
            new Token(TokenType.OpenParen, "(",new SchemeVoid(), 0),
            new Token(TokenType.Identifier, "define",new SchemeVoid(), 0),
            new Token(TokenType.String, "hello",new SchemeString("hello"), 0),
            new Token(TokenType.CloseParen, ")",new SchemeVoid(), 0),
        ])
    })

    test('single string delimiter', () => {
        expect(() => lexer.lex("(define \"hello)")).toThrow()
    })

    test('numbers', () => {
        expect(lexer.lex("1 2 3 4")).toStrictEqual([
            new Token(TokenType.Number, "1",new SchemeNumber(1), 0),
            new Token(TokenType.Number, "2",new SchemeNumber(2), 0),
            new Token(TokenType.Number, "3",new SchemeNumber(3), 0),
            new Token(TokenType.Number, "4", new SchemeNumber(4), 0),
        ])
    })

    test("multiple lines", ()=> {
        expect(lexer.lex("(a b)\n(c d)")).toStrictEqual([
            new Token(TokenType.OpenParen, "(",new SchemeVoid(), 0),
            new Token(TokenType.Identifier, "a",new SchemeVoid(), 0),
            new Token(TokenType.Identifier, "b",new SchemeVoid(), 0),
            new Token(TokenType.CloseParen, ")",new SchemeVoid(), 0),
            new Token(TokenType.OpenParen, "(",new SchemeVoid(), 1),
            new Token(TokenType.Identifier, "c",new SchemeVoid(), 1),
            new Token(TokenType.Identifier, "d",new SchemeVoid(), 1),
            new Token(TokenType.CloseParen, ")",new SchemeVoid(), 1),
        ])
    })

    test("newline should make new identifiers", () => {
        expect(lexer.lex("a\nb\nc")).toStrictEqual([
            new Token(TokenType.Identifier, "a", new SchemeVoid(), 0),
            new Token(TokenType.Identifier, "b", new SchemeVoid(), 1),
            new Token(TokenType.Identifier, "c", new SchemeVoid(), 2),
        ])
    })

    test("characters", () => {
        expect(lexer.lex("#\\newline")).toStrictEqual([
            new Token(TokenType.Char, "#\\newline", new SchemeCharacter("\n"), 0)
        ])
    })

    test("invalid character", () => {
        expect(() => lexer.lex('#\\')).toThrow()
    })

    test("invalid character 2", () => {
        expect(() => lexer.lex('#\\abcd')).toThrow()
    })

    test("single character", () => {
        expect(lexer.lex('#\\a')).toStrictEqual([new Token(TokenType.Char, "#\\a", new SchemeCharacter("\a"), 0)])
    })

	test("number decimal", () => {
        expect(lexer.lex("3.1415")).toStrictEqual([
            new Token(TokenType.Number, "3.1415",
					  new SchemeNumber(3.1415), 0)
        ])
    })

});
