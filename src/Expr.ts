import {ExprVisitor} from "./visitors/Visitor";
import {SchemeObject, SchemeProcedure, SchemeVoid} from "./Datatype";
import {make_var_token, Token, TokenType} from "./Token";
import {InterpreterError} from "./Error";
import {Environment} from "./Environment";

export abstract class Expr {
    abstract accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType;
}

export enum NumericOperation {
    Add,
    Sub,
    Mul,
    Div,
    IntDiv,
    Mod,
}

export const NumericFunctionMap: Map<
    NumericOperation,
(a: number, b: number) => number> = new Map([
    [NumericOperation.Add, (a, b)=>a+b],
    [NumericOperation.Sub, (a, b)=>a-b],
    [NumericOperation.Mul, (a, b)=>a*b],
    [NumericOperation.Div, (a, b)=> {
        if(b === 0) {
            throw new InterpreterError("Divide by zero")
        } else {
            return a / b
        }
    }],
    [NumericOperation.IntDiv, (a, b)=>{
        if(b === 0) {
            throw new InterpreterError("Divide by zero")
        } else {
            return Math.floor(a/b)
        }
    }],
    [NumericOperation.Mod, (a, b)=>a%b]
])

export function make_numeric_functions(): Map<string, SchemeProcedure> {
    let map = new Map()
    const make_fn = (str: string, num_op: NumericOperation) => {
        map.set(str, new SchemeProcedure(
            {
                parameters: [make_var_token("a"), make_var_token("b")],
                body: new NumericExpr(num_op, make_var_expr("a"), make_var_expr("b")),
                environment: new Environment()
            }))
    }
    make_fn("+", NumericOperation.Add)
    make_fn("-", NumericOperation.Sub)
    make_fn("*", NumericOperation.Mul)
    make_fn("/", NumericOperation.Div)
    make_fn("quotient", NumericOperation.IntDiv)
    make_fn("modulo", NumericOperation.Mod)
    return map
}

export class NumericExpr implements Expr {
    operator: NumericOperation;
    op1: Expr;
    op2: Expr;

    constructor(operator: NumericOperation, op1: Expr, op2: Expr) {
        this.operator = operator
        this.op1 = op1
        this.op2 = op2
    }

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
        return visitor.visitNumericExpr(this);
    }
}

export class PrintExpr implements Expr {
    expr: Expr
    constructor(expr: Expr) {
        this.expr = expr
    }

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
        return visitor.visitPrintExpr(this);
    }
}

export class LiteralExpr implements Expr {
    value: SchemeObject

    constructor(value: SchemeObject) {
        this.value = value
    }

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
        return visitor.visitValueExpr(this);
    }
}

export function make_var_expr(name: string, line_num: number = 0): VariableExpr {
    return new VariableExpr(new Token(TokenType.Identifier, name, new SchemeVoid(), line_num))
}

export class VariableExpr implements Expr {
    name: Token
    constructor(name: Token) {
        this.name = name
    }

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
        return visitor.visitVariableExpr(this)
    }

	static construct(name: string, line_num: number = 0): VariableExpr {
		return new VariableExpr(new Token(TokenType.Identifier, name, new SchemeVoid(), line_num))
	}
}

export class ProcedureCallExpr implements Expr {
    operator: Expr
    operands: Array<Expr>

    constructor(operator: Expr, operands: Array<Expr>) {
        this.operator=operator
        this.operands=operands
    }

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
        return visitor.visitProcedureCallExpr(this)
    }
}

export class AssignmentExpr implements Expr {
	name: Token
	expr: Expr

	constructor(name: Token, expr: Expr) {
		this.name = name
		this.expr = expr
	}
	
    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
		return visitor.visitAssignmentExpr(this)
    }
}

export class ConditionalExpr implements Expr {
    predicate: Expr
    conditional: Expr
    alternate?: Expr

    constructor(predicate: Expr, conditional: Expr, alternate?: Expr) {
        this.predicate=predicate
        this.conditional=conditional
        this.alternate=alternate
    }

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
        return visitor.visitIfExpr(this)
    }
}

export class LambdaExpr implements Expr {
	parameters: Array<Token>
	body: Expr

	constructor(parameters: Array<Token>, body: Expr) {
		this.parameters = parameters
		this.body = body
	}

	accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
		return visitor.visitLambdaExpr(this);
	}
}

/** @class Definitions. Not technically an expression and should only be instantiated when define is used in a proper position. */
export class DefinitionExpr implements Expr {
	name: Token
	body: Expr

    accept<ReturnType>(visitor: ExprVisitor<ReturnType>): ReturnType {
		return visitor.visitDefineExpr(this);
    }
	
	constructor(name: Token, body: Expr) {
		this.name = name
		this.body = body
	}
}
