import {Token, TokenType, TokenMap} from "./Token";
import {LexError} from "./Error";
import {SchemeBoolean, SchemeCharacter, SchemeObject, SchemeNumber, SchemeString, SchemeVoid} from "./Datatype";

const Delimiters: Set<string> = new Set([";", "(", ")", " ", "\"", "\n"])

const CharMap = new Map<string, string>([
    ["#\\newline", "\n"],
    ["#\\space", " "],
    ["#\\(", "("]
])

export abstract class ILexer {
    abstract lex(src: string): Array<Token>;
}

export class Lexer implements ILexer {
    source: string = ""
    pointer: number
    line_num: number

    constructor() {
        this.pointer = 0
        this.line_num = 0
    }

    lex(src: string): Array<Token> {
        this.source=src
        this.pointer=0
        this.line_num=0
        let tokens: Array<Token> = []
        while (!this.at_EOF()) {
            if (this.seek_to_next_token()) {
                return tokens
            }
            let token = this.next_token()
            tokens.push(token)
        }
        return tokens
    }

    /** Lex's the next token
     */
    next_token(): Token {

        const one_char = this.peek_n_char(1)
        const one_char_token = TokenMap.get(one_char)
        if (one_char_token !== undefined) {
            this.pointer += 1
            return new Token(one_char_token, one_char, new SchemeVoid(), this.line_num)
        }

        const two_char = this.peek_n_char(2)
        const two_char_token = TokenMap.get(two_char)
        if (two_char_token !== undefined) {
            this.pointer += 2
            let value: SchemeObject = new SchemeVoid()
            if(two_char_token === TokenType.True) {
                value = new SchemeBoolean(true)
            } else if (two_char_token === TokenType.False) {
                value = new SchemeBoolean(false)
            }
            return new Token(two_char_token, two_char, value, this.line_num)
        }

        const source_string = this.get_token_string()
        if (source_string.startsWith("#\\")) {

            let value: string | undefined = CharMap.get(source_string)
            if(value === undefined) {
                if(source_string.length !== 3) {
                    throw new LexError("Invalid character", this)
                } else {
                    value = source_string[2]
                }
            }

            return new Token(TokenType.Char, source_string, new SchemeCharacter(value), this.line_num)
        }
        if (!isNaN(parseFloat(source_string))) {
            return new Token(TokenType.Number, source_string, new SchemeNumber(parseFloat(source_string)), this.line_num)
        }
        if (this.peek_char() == "\"") {
            const str = this.seek_to_string_end()
            return new Token(TokenType.String, str, new SchemeString(str), this.line_num)
        }
        return new Token(TokenType.Identifier, source_string, new SchemeVoid, this.line_num)
    }

    /** Seeks to the next token
     Effects: Modifies the pointer and line number so that they point to the next non-whitespace, non-comment character in the source code
     @return boolean whether the function has seeked to the end of the file
     */
     seek_to_next_token(): boolean {

        if (this.at_EOF()) {
            return true
        }

        while (this.peek_char() === " " ||
        this.peek_char() === "\n" ||
        this.peek_char() === "\t" ||
        this.peek_char() === ";") {
            const c = this.get_char()

            if (c === "\n") {
                this.line_num += 1
            } else if (c === ";") {
                this.line_num += 1
                while (this.get_char() !== "\n") {
                    if (this.at_EOF()) {
                        return true
                    }
                }
            }

            if (this.at_EOF()) {
                return true
            }

        }
        return false
    }

    /** @method Lexer gets the string that is the lexeme of the next token.
     *  Effects: moves the pointer to the first whitespace after the token, or at the EOF.
     *  @return {string} The token lexeme
     */
     get_token_string(): string {
        const start = this.pointer
        while (!Delimiters.has(this.get_char())) {
            if (this.at_EOF()) {
                return this.source.substring(start, this.pointer)
            }
        }
        // Backtrack by 1
        this.pointer -= 1
        return this.source.substring(start, this.pointer)
    }

    /** Returns a single character and then advances the pointer */
     get_char(): string {
        const c = this.source[this.pointer]
        this.pointer += 1
        return c
    }

     peek_char(): string {
        return this.source[this.pointer]
    }

     peek_n_char(n: number): string {
        return this.source.substring(this.pointer, this.pointer + n)
    }

     seek_to_string_end(): string {
        if (this.get_char() !== "\"") {
            throw new LexError("Not a string", this)
        }
        const start = this.pointer
        while (this.get_char() !== "\"") {
            if (this.at_EOF()) {
                throw new LexError("Unexpected EOF while seeking to end of string", this)
            }
        }
        // Don't include the ending quote
        return this.source.substring(start, this.pointer - 1)
    }

     at_EOF() {
        return this.pointer === (this.source.length)
    }
}
