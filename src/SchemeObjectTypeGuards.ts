import {SchemeNumber, SchemeObject, SchemeProcedure} from "./Datatype";

export function is_numeric(obj: SchemeObject): obj is SchemeNumber {
    return typeof obj.implValue === "number"
}

export function object_is_procedure(obj: SchemeObject): obj is SchemeProcedure {
    return obj.implValue.parameters !== undefined &&
        obj.implValue.body !== undefined &&
        obj.implValue.environment !== undefined
}