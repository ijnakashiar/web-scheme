import {Token} from "./Token";
import {Expr} from "./Expr";
import {IImmutableEnvironment, Environment} from "./Environment";
import { Lexer } from "./Lexer";
import { Parser } from "./Parser";
import { Clonable } from "./traits/Clonable";
import { ExprCloner } from "./visitors/ExprCloner";

export abstract class SchemeObject {
	implValue: any
    abstract print_value(): string
	abstract clone(): SchemeObject
}


export class SchemeBoolean implements SchemeObject {
    implValue: boolean;

    constructor(value: boolean) {
        this.implValue = value
    }

    print_value(): string {
        return this.implValue? "#t":"#f";
    }

	clone(): SchemeBoolean {
		return new SchemeBoolean(this.implValue)
	}
}

export class SchemeNumber implements SchemeObject {
    implValue: number;
	constructor(value: number) {
        this.implValue = value
    }

    print_value(): string {
        return this.implValue.toString()
    }

	clone(): SchemeNumber {
		return new SchemeNumber(this.implValue)
	}
}

export class SchemeExactNumber implements SchemeObject {
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }
    print_value(): string {
        throw new Error("Method not implemented.");
    }
    implValue: any;
	
}

export class SchemeInexactNumber implements SchemeObject {
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }
    print_value(): string {
        throw new Error("Method not implemented.");
    }
    implValue: any;
	
}

export class SchemeString implements SchemeObject {
    implValue: string;

    constructor(str: string) {
        this.implValue=str
    }
	clone(): SchemeString {
		return new SchemeString(this.implValue)
	}
    print_value(): string {
        return this.implValue
    }
}

export class SchemeCharacter implements SchemeObject {
    implValue: string;
	constructor(char: string) {
        if(char.length !== 1) {
            throw new Error("Character is not one character long")
        }
        this.implValue = char
    }
    clone(): SchemeObject {
		throw new Error("Method not implemented.");
    }

    print_value(): string {
        throw new Error("Method not implemented.");
    }
}

export class SchemeSymbol implements SchemeObject {
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }
    print_value(): string {
        throw new Error("Method not implemented.");
    }
    implValue: any;
	
}

export class SchemeVector implements SchemeObject {
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }
    print_value(): string {
        throw new Error("Method not implemented.");
    }
    implValue: any;
	
}

export type SchemeFunctionType = {
    parameters: Array<Token>
    body: Expr
    environment: IImmutableEnvironment
}

export class SchemeProcedure implements SchemeObject {
    implValue: SchemeFunctionType

    constructor(funV: SchemeFunctionType) {
        this.implValue = funV
    }
    clone(): SchemeObject {
        return new SchemeProcedure({
			parameters: this.implValue.parameters.map((param)=>param.clone()),
			body: ExprCloner.clone(this.get_body()),
			environment: this.implValue.environment.clone()
		})
    }

    get_names() {
        return this.implValue.parameters.map((param)=>param.lexeme)
    }

    get_body() {
        return this.implValue.body
    }

    param_len() {
        return this.implValue.parameters.length
    }

    extend_env(values: Array<SchemeObject>) {
        return this.implValue.environment.add_multiple(this.get_names(), values)
    }

    print_value(): string {
        return JSON.stringify(this.implValue, null, 2);
    }

	static construct(params: Array<string>, body_src: string, env: Environment = new Environment()): SchemeProcedure {
		const parameters = params.map((name)=>Token.make_var(name))
		const body = (new Parser().parse(new Lexer().lex(body_src)))[0]
		return new SchemeProcedure({
			parameters:parameters,
			body:body,
			environment:env
		})
	}
}

export class SchemeVoid implements SchemeObject {
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }
    print_value(): string {
        throw new Error("Method not implemented.");
    }
    readonly implValue = null
}
