import {SchemeObject} from "./Datatype";

export abstract class SchemePort implements SchemeObject {
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }
    print_value(): string {
        throw new Error("Method not implemented.");
    }
    implValue: any;
}

export class SchemeInputPort implements SchemePort {
    implValue: InputSource;
    constructor(inputSource: InputSource) {
        this.implValue=inputSource
    }
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }

    print_value(): string {
        return "";
    }
}

export abstract class InputSource {
    abstract read(n: number): string
    abstract peek(): string
    abstract at_eof(): boolean
    abstract isReady(): boolean
}

export class SchemeOutputPort implements SchemePort {
    implValue: OutputSource;
    constructor(outputSource: OutputSource) {
        this.implValue=outputSource
    }
    clone(): SchemeObject {
        throw new Error("Method not implemented.");
    }

    print_value(): string {
        return "";
    }

	access(): OutputSource {
		return this.implValue
	}
}

export abstract class OutputSource {
    abstract write(str: string): void
    abstract newline(): void
}

export class ConsoleOutput implements OutputSource {
    newline(): void {
        console.log("\n")
    }

    write(str: string): void {
        console.log(str)
    }
}

export class NodeFileOutput implements OutputSource {
    newline(): void {

    }

    write(str: string): void {

    }
}
