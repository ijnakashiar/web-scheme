export type Optional<T> = {
    value: T | null,
    error: string
}

export function is_empty<T>(opt: Optional<T>) {
    return opt === null
}

export function get_error_message<T>(opt: Optional<T>) {
    if(is_empty(opt)) {
        return opt.error
    } else {
        return ""
    }
}

export function dispatch<T>(opt: Optional<T>, func: (_:T)=> Optional<T>) {
    if(opt.value === null) {
        return opt
    } else {
        return func(opt.value)
    }
}