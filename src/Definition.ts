import { Expr } from "./Expr";
import { Token } from "./Token";


/** @class Scheme defines */
class Definition {
	name: Token
	expr: Expr

	constructor(name: Token, expr: Expr) {
		this.name = name
		this.expr = expr
	}
}
