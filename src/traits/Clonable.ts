export abstract class Clonable<T> {
	abstract clone(): T
}
