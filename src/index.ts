import {FileInput} from "./FileInput"
import {ILexer, Lexer} from "./Lexer";
import {IParser, Parser} from "./Parser"
import {IInterpreter, SchemeInterpreter} from "./Interpreter";
import {SchemeObject} from "./Datatype";
import { ASTPrinter } from "./visitors/ASTPrinter";

const fi = new FileInput();
if (process.argv.length === 3) {
    fi.readInput(process.argv[2])
        .then((contents) => {
            const lexer: ILexer = new Lexer()
            const parser: IParser = new Parser()
            const ast_printer: IInterpreter<string> = new ASTPrinter()
            const evaluator: IInterpreter<SchemeObject> = new SchemeInterpreter()

            const tokens = lexer.lex(contents)
            const expressions = parser.parse(tokens)
            const values = evaluator.interpret(expressions)

            for(let value of values) {
                console.log(value.print_value())
            }
        })
        .catch((err) => console.log(err))
}

