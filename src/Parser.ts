import {Token, TokenType} from "./Token";
import {ConditionalExpr, DefinitionExpr, Expr, LambdaExpr, LiteralExpr, ProcedureCallExpr, VariableExpr} from "./Expr";
import {ParseError} from "./Error";

const Keywords = new Set([
	"lambda",
	"quote",
	"if",
	"set!",
	"cond",
	"case",
	"and",
	"or",
	"let",
	"let*",
	"letrec",
	"begin",
	"do",
	"delay",
	"define"
])

export abstract class IParser {
	abstract parse(tokens: Array<Token>): Array<Expr>;
}

/**
Scheme parser
 */
export class Parser implements IParser {

	tokens: Array<Token> = []
	pointer: number
	private readonly LiteralTypes = new Set([
		TokenType.True,
		TokenType.False,
		TokenType.Number,
		TokenType.Char,
		TokenType.String
	])

	peek_current_token(): Token {
		if (this.at_end()) {
			throw new ParseError("Unexpected end of file when parsing expression", this)
		}
		return this.tokens[this.pointer]
	}

	get_current_token(): Token {
		if (this.at_end()) {
			throw new ParseError("Unexpected end of file when parsing expression", this)
		}
		return this.tokens[this.pointer++]
	}

	/** Look n tokens ahead of the current token. Current token is the 0th token. */
	peek_nth_token(n: number): Token {
		if (this.pointer + n < this.tokens.length) {
			return this.tokens[this.pointer + n]
		} else {
			throw new ParseError("Unexpected end of tokens", this)
		}
	}

	compare_current_token(tt: TokenType): boolean {
		return tt === this.peek_current_token().tokenType
	}

	is_keyword(lexeme: string): boolean {
		return Keywords.has(lexeme)
	}

	check_operator(lexeme: string): boolean {
		return this.compare_current_token(TokenType.OpenParen) &&
			!this.at_end() &&
			this.peek_nth_token(1).lexeme === lexeme
	}

	/** Returns true if the current token is one of the types in the parameter
		
		@param types The set of types to compare against
		@returns boolean whether the current token exists in the set
	 */
	current_token_is_type(types: Set<TokenType>): boolean {
		return types.has(this.peek_current_token().tokenType)
	}

	constructor() {
		this.pointer = 0
	}

	at_end(): boolean {
		return this.pointer === this.tokens.length
	}

	/** Parses the list of tokens
  @returns An array of expressions
	 */
	parse(tokens: Array<Token>): Array<Expr> {
		this.tokens = tokens
		this.pointer = 0
		let expressions: Array<Expr> = []
		while (!this.at_end()) {
			if (this.compare_current_token(TokenType.CloseParen)) {
				throw new Error("Unbalanced parentheses")
			}
			const expr = this.parse_literal()
			expressions.push(expr)
		}
		return expressions
	}

	parse_literal(): Expr {

		if (this.current_token_is_type(this.LiteralTypes)) {
			const value = this.get_current_token().value
			if (value === null) {
				throw new ParseError("Token is empty", this)
			} else {
				return new LiteralExpr(value)
			}

		} else if (this.compare_current_token(TokenType.Quote)) {
			// Consume the quotation
			this.get_current_token()

			const value = this.get_current_token().value
			if (value === null) {
				throw new ParseError("Token is empty", this)
			}
			return new LiteralExpr(value)

		} else {
			return this.parse_identifier()
		}
	}

	parse_identifier(): Expr {
		if (this.compare_current_token(TokenType.Identifier)) {
			return new VariableExpr(this.get_current_token())
		} else {
			return this.parse_lambda()
		}
	}

	parse_lambda(): Expr {
		if(!this.check_operator("lambda")) {
			return this.parse_conditional()
		}
		this.get_current_token()
		this.get_current_token()

		let args: Array<Token> = []

		if (!this.compare_current_token(TokenType.OpenParen)) {

			if (!this.compare_current_token(TokenType.Identifier)) {
				throw new ParseError("Parameters of a lambda need to be identifiers", this)
			}

			args.push(this.get_current_token())

		} else {
			this.get_current_token()

			while (!this.compare_current_token(TokenType.CloseParen)) {

				if (!this.compare_current_token(TokenType.Identifier)) {
					throw new ParseError("Parameters of a lambda need to be identifiers", this)
				}

				args.push(this.get_current_token())
			}
			this.get_current_token()
		}

		const body = this.parse_literal()
		if (!this.compare_current_token(TokenType.CloseParen)) {
			throw new ParseError("Expected closing parentheses", this)
		}
		this.get_current_token()
		return new LambdaExpr(args, body)
	}

	parse_conditional(): Expr {
		if (!this.check_operator("if")) {
			return this.parse_definition_expr()
		}

		this.get_current_token()
		this.get_current_token()

		const test = this.parse_literal()
		const consequent = this.parse_literal()
		let alternate = undefined
		if (!this.compare_current_token(TokenType.CloseParen)) {
			alternate = this.parse_literal()
		}
		if (!this.compare_current_token(TokenType.CloseParen)) {
			throw new ParseError("Expected closing parentheses", this)
		}
		this.get_current_token()
		return new ConditionalExpr(test, consequent, alternate)
	}

	parse_definition_expr(): Expr {
		if (!this.check_operator("define")){
			return this.parse_procedure_call()
		}

		this.get_current_token()
		this.get_current_token()

		if(this.compare_current_token(TokenType.OpenParen)) {
			this.get_current_token()
			const name = this.get_current_token()
			let args = []
			while (!this.compare_current_token(TokenType.CloseParen)) {

				if (!this.compare_current_token(TokenType.Identifier)) {
					throw new ParseError("Parameters of a lambda need to be identifiers", this)
				}

				args.push(this.get_current_token())
			}

			if (!this.compare_current_token(TokenType.CloseParen)) {
				throw new ParseError("Expected closing parentheses", this)
			}
			this.get_current_token()

			const body = this.parse_literal()

			if (!this.compare_current_token(TokenType.CloseParen)) {
				throw new ParseError("Expected closing parentheses", this)
			}			
			this.get_current_token()
			return new DefinitionExpr(name, new LambdaExpr(args, body))
		} else {
			const name = this.get_current_token()
			const body = this.parse_literal()
			if (!this.compare_current_token(TokenType.CloseParen)) {
				throw new ParseError("Expected closing parentheses", this)
			}
			this.get_current_token()

			return new DefinitionExpr(name, body)
		}
	}


	parse_procedure_call(): Expr {
		if (!this.compare_current_token(TokenType.OpenParen)) {
			throw new ParseError("Unknown type", this)
		}
		this.get_current_token()
		const operator = this.parse_literal()
		let args = []
		while (!this.compare_current_token(TokenType.CloseParen)) {
			args.push(this.parse_literal())
		}
		this.get_current_token()

		return new ProcedureCallExpr(operator, args)
	}

}

