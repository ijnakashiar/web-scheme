import fs, { readFile } from "fs-extra";

/** Reads input from a file.
 */
export class FileInput {
	/** Reads input from a file and returns it in the form of an asynchronous string.
	 */
	readInput(file_path: string) : Promise<string> {
		return readFile(file_path, 'utf8');
	}
}
