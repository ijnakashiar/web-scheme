import {Parser} from "./Parser";
import {Lexer} from "./Lexer";

export class ParseError implements Error {
    name: string = "ParserError";
    message: string;
    stack?: string | undefined;
    cause?: unknown;

    constructor(message: string, parser: Parser) {
        this.message = `An error occurred during parsing:
    ${message}
    Parser state: ${JSON.stringify(parser, null, 2)}`
    }
}

export class InternalError {
    message: string

    constructor(message: string) {
        this.message = message
    }
}

export class NotFoundError implements Error {
    message: string;
    name: string = "NotFoundError"

    constructor(message: string) {
        this.message = message
    }
}

export class InterpreterError implements Error {
    message: string;
    name: string = "InterpreterError"

    constructor(message: string) {
        this.message = message
    }
}

export class LexError implements Error {
    name: string = "LexError";
    message: string;
    stack?: string | undefined;
    cause?: unknown;

    constructor(message: string, lexer: Lexer) {
        this.message = "An error occurred during scanning:\n" + message +
            "\nOccurred on line " + lexer.line_num +
            "\nEOF reached: " + lexer.at_EOF()
    }
}
