import {SchemeObject, SchemeVoid} from "./Datatype"

/** An enumeration of all the possible Token types. */
export enum TokenType {
  Identifier="Identifier",
  True="True",
  False="False",
  Number="Number",
  Char="Char",
  String="String",
  OpenParen="OpenParen",
  CloseParen="CloseParen",
  /** Denotes a vector */
  PoundOpenParen="PoundOpenParen",
  Quote="Quote",
  QuasiQuote="QuasiQuote",
  Comma="Comma",
  ListComma="ListComma",
  Dot="Dot",
}

export const TokenMap: Map<string, TokenType> = new Map<string, TokenType>([
	["#t", TokenType.True],
	["#f", TokenType.False],
	["(",  TokenType.OpenParen],
	[")",  TokenType.CloseParen],
	["#(",  TokenType.PoundOpenParen],
	[".", TokenType.Dot],
	[",", TokenType.Comma],	
	["@,", TokenType.ListComma],
	["'", TokenType.Quote],	
	["`", TokenType.QuasiQuote],	
])

export function make_var_token(name: string, line_num: number = 0) {
	return new Token(TokenType.Identifier, name, new SchemeVoid(), line_num)
}

/** The Token class is the symbolic representation of a piece of source code, with some metadata.
 */
export class Token {
	readonly tokenType: TokenType
	/** Literal string in the source code */
	readonly lexeme: string
	readonly value: SchemeObject
	readonly line: number

	/** Constructor for the Token data type.
		@param tokenType The type of Token.
		@param lexeme The literal text in the source code that the Token represents.
		@param value The canonical value of the Token in terms of Scheme data types.
		@param line The line number where this Token is from.
	 */
	constructor(tokenType: TokenType, lexeme: string, value: SchemeObject, line: number) {
		this.tokenType = tokenType
		this.lexeme = lexeme
		this.value = value
		this.line = line
	}

	static make_var(lexeme: string): Token {
		return new Token(TokenType.Identifier, lexeme, new SchemeVoid(), 0)
	}

  clone() {
	return new Token(this.tokenType, this.lexeme, this.value, this.line)
  }
}
