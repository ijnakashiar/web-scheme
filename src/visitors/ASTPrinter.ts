import { AssignmentExpr, ConditionalExpr, DefinitionExpr, Expr, LambdaExpr, LiteralExpr, NumericExpr, PrintExpr, ProcedureCallExpr, VariableExpr } from "../Expr"
import { IInterpreter } from "../Interpreter"
import { ExprVisitor } from "./Visitor"


export class ASTPrinter extends IInterpreter<string> implements ExprVisitor<string> {
    visitDefineExpr(expr: DefinitionExpr): string {
        throw new Error("Method not implemented.")
    }

	evaluate(expr: Expr): string {
		return expr.accept(this)
	}

	visitAssignmentExpr(expr: AssignmentExpr): string {
		return `(set! ${expr.name.lexeme} ${this.evaluate(expr.expr)})`
	}

	visitLambdaExpr(expr: LambdaExpr): string {
		return `LAMBDA[ 
PARAMETERS[${expr.parameters.map((tkn)=>tkn.lexeme)}] 
BODY[${this.evaluate(expr.body)}]]`
	}

	visitIfExpr(expr: ConditionalExpr): string {
		return `IF[${this.evaluate(expr.predicate)} 
${this.evaluate(expr.conditional)}
${expr.alternate !== undefined ? this.evaluate(expr.alternate) : ""}]`
	}

	visitProcedureCallExpr(expr: ProcedureCallExpr): string {
		return `CALL[
OPERATOR[${this.evaluate(expr.operator)}]
OPERANDS[${this.interpret(expr.operands)}]]`
	}

	visitVariableExpr(expr: VariableExpr): string {
		return `VARIABLE[${expr.name.lexeme}]`
	}

	visitValueExpr(expr: LiteralExpr): string {
		return `VALUE[${expr.value.implValue}]`
	}

	visitNumericExpr(expr: NumericExpr): string {
		return "[numeric expr]";
	}

	visitPrintExpr(expr: PrintExpr): string {
		return "[print expr]";
	}
}
