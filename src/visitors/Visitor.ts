import {ProcedureCallExpr, ConditionalExpr, LambdaExpr, NumericExpr, PrintExpr, LiteralExpr, VariableExpr, AssignmentExpr, DefinitionExpr} from "../Expr";

export abstract class ExprVisitor<ReturnType> {
    abstract visitAssignmentExpr(expr: AssignmentExpr): ReturnType
    abstract visitLambdaExpr(expr: LambdaExpr): ReturnType
    abstract visitNumericExpr(expr: NumericExpr): ReturnType
    abstract visitPrintExpr(expr: PrintExpr): ReturnType
    abstract visitValueExpr(expr: LiteralExpr): ReturnType
    abstract visitVariableExpr(expr: VariableExpr): ReturnType
    abstract visitProcedureCallExpr(expr: ProcedureCallExpr): ReturnType
    abstract visitIfExpr(expr: ConditionalExpr): ReturnType
	abstract visitDefineExpr(expr: DefinitionExpr): ReturnType
}
