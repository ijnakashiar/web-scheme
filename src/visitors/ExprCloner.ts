import { AssignmentExpr, ConditionalExpr, DefinitionExpr, Expr, LambdaExpr, LiteralExpr, NumericExpr, PrintExpr, ProcedureCallExpr, VariableExpr } from "../Expr";
import { ExprVisitor } from "./Visitor";

/** @class Performs a deep copy of the expression.
	Some variables in the expression are not deep copied which may cause some issues
 */
export class ExprCloner implements ExprVisitor<Expr> {
    visitDefineExpr(expr: DefinitionExpr): Expr {
        throw new Error("Method not implemented.");
    }
	static clone(expr: Expr): Expr {
		return expr.accept(new ExprCloner())
	}

    visitAssignmentExpr(expr: AssignmentExpr): Expr {
        return new AssignmentExpr(expr.name, expr.expr.accept(this))
    }
    visitLambdaExpr(expr: LambdaExpr): Expr {
        return new LambdaExpr(expr.parameters, expr.body.accept(this))
    }
    visitNumericExpr(expr: NumericExpr): Expr {
        return new NumericExpr(expr.operator,
							   expr.op1.accept(this),
							   expr.op2.accept(this))
    }
    visitPrintExpr(expr: PrintExpr): Expr {
        return new PrintExpr(expr.expr.accept(this))
    }
    visitValueExpr(expr: LiteralExpr): Expr {
        return new LiteralExpr(expr.value.clone())
    }
    visitVariableExpr(expr: VariableExpr): Expr {
        return new VariableExpr(expr.name);
    }
    visitProcedureCallExpr(expr: ProcedureCallExpr): Expr {
        return new ProcedureCallExpr(
			expr.operator.accept(this),
			expr.operands.map((rand) => rand.accept(this)));
    }
    visitIfExpr(expr: ConditionalExpr): Expr {
        return new ConditionalExpr(expr.predicate.accept(this),
								   expr.conditional.accept(this),
								   (expr.alternate!==undefined)?
			expr.alternate.accept(this) :
			undefined);
    }
	
}
