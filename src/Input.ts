/// 
export abstract class InputHandler {
	handle(input: string) {
		
	}
}

export abstract class Input {
	input_handlers: InputHandler[] = []
	input: string = ""

	constructor() {
		
	}

	on_input_send() {
		for(let handler of this.input_handlers) {
			handler.handle(this.input);
		}
	}
}
