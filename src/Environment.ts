import {SchemeObject} from "./Datatype";
import {InterpreterError, NotFoundError} from "./Error";

/** @class IImmutableEnvironment An environment that does not get modified
 */
export abstract class IImmutableEnvironment {
	/** Gets the object associated with the given name
	 *  @param name The name of the object
	 *  @return SchemeObject the object that is associated with the name
	 *  @throws NotFoundError if the object
	 */
	abstract get(name: string): SchemeObject

	/**
	 * Returns a new environment with the added object
	 * @param name The name of the object
	 * @param obj The value of the object
	 */
	abstract add(name: string, obj: SchemeObject): IImmutableEnvironment

	/**
	 * Returns a new environment with multiple objects
	 * @param names The names of the object
	 * @param objs The values of the object
	 */
	abstract add_multiple(names: Array<string>, objs: Array<SchemeObject>): IImmutableEnvironment

	abstract clone(): IImmutableEnvironment
}

abstract class IMutableEnvironment {}

export const PredefinedEnvironmentMap: Map<string, SchemeObject> = new Map([])

export class Environment implements IImmutableEnvironment {
	readonly env: Map<string, SchemeObject>

	constructor(map?: Map<string, SchemeObject>) {
		this.env = new Map(map)
	}

	add(name: string, obj: SchemeObject): IImmutableEnvironment {
		const env = this.clone()
		env.env.set(name, obj)
		return env
	}

	add_multiple(names: Array<string>, objs: Array<SchemeObject>): IImmutableEnvironment {
		if(names.length !== objs.length) {
			throw new InterpreterError("Names and objects are not the same lengths")
		}
		const env = this.clone()
		names.forEach((n, i) => env.env.set(n, objs[i].clone()))
		return env
	}

	get(name: string): SchemeObject {
		const val = this.env.get(name)
		if(val === undefined) {
			throw new NotFoundError("Undefined reference to variable: " + name)
		} else {
			return val
		}
	}

	clone(): Environment {
		let env = new Map<string, SchemeObject>()
		this.env.forEach((value, key) => {
			env.set(key, value.clone())
		})
		return new Environment(env)
	}
}
