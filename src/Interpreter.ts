import {ExprVisitor} from "./visitors/Visitor";
import {
	ProcedureCallExpr,
	Expr,
	NumericExpr,
	PrintExpr,
	LiteralExpr,
	VariableExpr,
	AssignmentExpr,
	ConditionalExpr,
	LambdaExpr, NumericFunctionMap, make_numeric_functions, DefinitionExpr
} from "./Expr";
import {SchemeObject, SchemeNumber, SchemeProcedure, SchemeVoid} from "./Datatype";
import {ConsoleOutput, SchemeInputPort, SchemeOutputPort} from "./SchemePort";
import {Environment, IImmutableEnvironment} from "./Environment";
import {InterpreterError} from "./Error";
import {is_numeric, object_is_procedure} from "./SchemeObjectTypeGuards";

export abstract class IInterpreter<ReturnType> {
	interpret(expressions: Array<Expr>): Array<ReturnType> {
		let res = []
		for (let expr of expressions) {
			res.push(this.evaluate(expr))
		}
		return res
	}

	abstract evaluate(expr: Expr): ReturnType
}

export class SchemeInterpreter extends IInterpreter<SchemeObject> implements ExprVisitor<SchemeObject> {

	currentInputPort: SchemeInputPort | null
	currentOutputPort: SchemeOutputPort | null
	environment: IImmutableEnvironment

	constructor(env: IImmutableEnvironment = new Environment(make_numeric_functions())) {
		super()
		this.currentInputPort = null
		this.currentOutputPort = new SchemeOutputPort(new ConsoleOutput());
		this.environment = env
	}

    visitDefineExpr(expr: DefinitionExpr): SchemeObject {
		this.environment = this.environment.add(expr.name.lexeme, this.evaluate(expr.body))
        return new SchemeVoid()
    }

	evaluate(expr: Expr): SchemeObject {
		return expr.accept(this)
	}

	visitAssignmentExpr(expr: AssignmentExpr): SchemeObject {
		throw new Error("Method not implemented.");
	}

	visitLambdaExpr(expr: LambdaExpr): SchemeObject {
		return new SchemeProcedure({
			parameters: expr.parameters,
			body: expr.body,
			environment: this.environment.clone() // Pass by value
		})
	}

	visitIfExpr(expr: ConditionalExpr): SchemeObject {
		const pred_value = this.evaluate(expr.predicate)
		if(pred_value.implValue !== false) {
			return this.evaluate(expr.conditional)
		} else if (expr.alternate !== undefined) {
			return this.evaluate(expr.alternate)
		} else {
			return new SchemeVoid()
		}
	}

	visitNumericExpr(expr: NumericExpr): SchemeObject {
		const val1 = this.evaluate(expr.op1)
		const val2 = this.evaluate(expr.op2)
		if(!is_numeric(val1)) {
			throw new InterpreterError("Expected number in argument 1")
		}
		if(!is_numeric(val2)) {
			throw new InterpreterError("Expected number in argument 2")
		}

		const value = NumericFunctionMap.get(expr.operator)!(val1.implValue, val2.implValue)

		return new SchemeNumber(value);
	}

	// Produces void type
	visitPrintExpr(expr: PrintExpr): SchemeObject {
		this.currentOutputPort?.access().write(this.evaluate(expr.expr).print_value())
		return new SchemeVoid();
	}

	visitValueExpr(expr: LiteralExpr): SchemeObject {
		return expr.value
	}

	visitProcedureCallExpr(expr: ProcedureCallExpr): SchemeObject {
		const operator_value = this.evaluate(expr.operator)
		if(!object_is_procedure(operator_value)) {
			throw new InterpreterError("Operator is not a procedure: " + operator_value.print_value())
		}
		if(expr.operands.length !== operator_value.implValue.parameters.length) {
			throw new InterpreterError("Operand length does not match")
		}
		const values = expr.operands.map((operand) => this.evaluate(operand))
		const new_env = operator_value.extend_env(values)
		const old_env = this.environment
		this.environment = new_env
		const value = this.evaluate(operator_value.get_body())
		this.environment = old_env
		return value
	}

	visitVariableExpr(expr: VariableExpr): SchemeObject {
		return this.environment.get(expr.name.lexeme)
	}

}


