import { Expr } from "../Expr";

export interface Pass {
	transform(expr: Array<Expr>): Array<Expr>
}
