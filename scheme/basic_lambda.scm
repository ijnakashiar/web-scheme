;; Expect to evaluate to 2
((lambda (a b c) b) 1 2 3)

;; Expect to evaluate to 10
(+ 1 (+ 2 (+ 3 4)))

;; Expect to print out the value of the procedure
(lambda (a b c) (+ 1 2))

;; Expect that the function produces "hello"
(((lambda (a) (lambda (b) a)) "hello") 0)