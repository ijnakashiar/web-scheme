(define hello "1")
(define (hello a b c)
  (+ 1 2) ; somethingh
  (let ((a b))
    (c a)))

(define hello "world")

;; Lambda
(lambda (a b c)
  (+ a (+ b c)))

;; Conditional
(if (= 2 3)
  5
  6)

;; Assignment (mutation)
(set! a 123)